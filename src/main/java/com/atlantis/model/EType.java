package com.atlantis.model;

public enum EType {
    BOOLEAN,
    FLOAT,
    STRING
}
