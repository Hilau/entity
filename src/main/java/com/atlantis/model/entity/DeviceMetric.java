package com.atlantis.model.entity;

import com.atlantis.model.EDeviceType;
import com.atlantis.model.EType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "device_metric")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceMetric implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_metric_id")
    private Long id;

    @Column(name = "device_metric_value")
    @JsonProperty("metricValue")
    private String value;

    @Column(name = "device_metric_date")
    @JsonProperty("metricDate")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private String date;

    @Column(name = "device_metric_unit")
    private String unit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="device_id")
    @JsonIgnore
    private Device device;
}
