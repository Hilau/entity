package com.atlantis.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "device_groups")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_group_id")
    private Long id;

    @Column(name = "device_group_name")
    private String name;

    @ManyToMany(mappedBy = "deviceGroups", fetch = FetchType.EAGER)
    @JsonBackReference
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "device_device_group",
            joinColumns = @JoinColumn(name = "device_group_id"),
            inverseJoinColumns = @JoinColumn(name = "device_id"))
    private Set<Device> devices;
}
