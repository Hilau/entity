package com.atlantis.model.entity;

import com.atlantis.model.EDeviceType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "device_type")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_type_id")
    @JsonIgnore
    private Long id;

    @Column(name = "device_type_value")
    @JsonProperty("deviceType")
    @Enumerated(EnumType.STRING)
    private EDeviceType type;
}
