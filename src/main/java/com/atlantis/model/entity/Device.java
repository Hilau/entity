package com.atlantis.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "devices")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Device implements Serializable {

    @Id
    @Column(name = "device_id")
    @JsonProperty("deviceId")
    private String id;

    @Column(name = "device_name")
    private String name;

    @Column(name = "device_location")
    private String location;

    @Column(name = "device_installation_date")
    private LocalDateTime installationDate;

    @ManyToOne
    @JoinColumn(name="device_type_id")
    private DeviceType type;

    @Column(name = "is_running")
    private boolean run;

    @ManyToMany(mappedBy = "devices", fetch = FetchType.EAGER)
    @JsonBackReference
    private Set<DeviceGroup> deviceGroup;
}
