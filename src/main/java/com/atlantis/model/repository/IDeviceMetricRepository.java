package com.atlantis.model.repository;

import com.atlantis.model.entity.DeviceMetric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IDeviceMetricRepository extends JpaRepository<DeviceMetric, Long> {
    DeviceMetric findFirstByDevice_IdOrderByDateDesc(String deviceId);

    List<DeviceMetric> findByDevice_Id(String deviceId);
}