package com.atlantis.model.repository;

import com.atlantis.model.entity.DeviceGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDeviceGroupRepository extends JpaRepository<DeviceGroup, Long> {
}
