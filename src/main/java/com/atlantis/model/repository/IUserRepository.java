package com.atlantis.model.repository;

import com.atlantis.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByFirstNameAndLastName(String firstName, String lastName);

    Optional<User> findUserByOid(String oid);

    Optional<User> findUserByUsernameAndPassword(String login, String password);
}
