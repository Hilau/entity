package com.atlantis.model.repository;

import com.atlantis.model.EDeviceType;
import com.atlantis.model.entity.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IDeviceTypeRepository extends JpaRepository<DeviceType, Long> {
    Optional<DeviceType> findByType(EDeviceType type);
}
