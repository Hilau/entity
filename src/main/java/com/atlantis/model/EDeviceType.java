package com.atlantis.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EDeviceType {
    @JsonProperty("presenceSensor")
    PRESENCE(""),
    @JsonProperty("temperatureSensor")
    TEMPERATURE("°C"),
    @JsonProperty("brightnessSensor")
    BRIGHTNESS("Lumen"),
    @JsonProperty("atmosphericPressureSensor")
    ATMOSPHERIC_PRESSURE("Pa"),
    @JsonProperty("humiditySensor")
    HUMIDITY("g/m3"),
    @JsonProperty("soundLevelSensor")
    SOUND_LEVEL("dB"),
    @JsonProperty("gpsSensor")
    GPS("lat/lon"),
    @JsonProperty("co2Sensor")
    CO2("ppm"),
    @JsonProperty("ledSensor")
    LED(""),
    @JsonProperty("beeperSensor")
    BEEPER("");

    private String unit;

    EDeviceType(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return this.unit;
    }
}
